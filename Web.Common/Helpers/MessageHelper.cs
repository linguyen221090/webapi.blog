﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Enum;

namespace Web.Common.Helpers
{
    public static class MessageHelper
    {
        public static string GetReturnMessage(DataActionType actionType)
        {
            switch (actionType)
            {
                case DataActionType.Success:
                    return "Successfully!";
                case DataActionType.Error:
                    return "Error!";
                default:
                    return "Server is unavailable, please contact your administrator!";
            }
        }
    }
}
