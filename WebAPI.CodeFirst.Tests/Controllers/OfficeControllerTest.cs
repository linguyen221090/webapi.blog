﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Web.Service;
using Web.Model.Models;
using Moq.Matchers;
using System.Web.Http.Results;
using WebAPI.CodeFirst.Controllers;

namespace WebAPI.CodeFirst.Tests.Controllers
{
    [TestClass]
    public class OfficeControllerTest
    {
        [TestMethod]
        public void OfficeGetOfficeByIDAction_LessAndEqual0_RenderBadRequest()
        {
            var _mockOffice = new Mock<IOfficeService>();
            var _officeController = new OfficeController(_mockOffice.Object);
            var result = _officeController.GetOfficeByID(0);

            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public void OfficeGetOfficeByIDAction_NoObject_RenderOk()
        {
            var _mockOffice = new Mock<IOfficeService>();
            var _officeController = new OfficeController(_mockOffice.Object);
            int id = 999;
            var actualResult = _officeController.GetOfficeByID(id);
            Assert.IsInstanceOfType(actualResult, typeof(OkResult));
        }

        [TestMethod]
        public void OfficeGetOfficeByIDService_HasObject_RenderObject()
        {
            var _mockOffice = new Mock<IOfficeService>();
            Office office = new Office()
            {
                ID = 1,
                Name = "Angular Tutor",
                Company = "Nash Tech",
                Address1 = "364 Cong Hoa Street"
            }
            ;
            _mockOffice.Setup(obj => obj.GetByID(It.IsAny<int>())).Returns(office);
            int id = 1;
            var actualResult = _mockOffice.Object.GetByID(id);
            var expectedResult = "Angular Tutor";
            Assert.AreEqual(actualResult.Name, expectedResult);
        }
    }
}
