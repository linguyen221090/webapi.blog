import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ContactService } from './shared/contact.service';
import { Contact } from './shared/contact.model';

import { Office } from '../admin/office/shared/office.model';
import { OfficeService } from '../admin/office/shared/office.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers: [ContactService, OfficeService]
})
export class ContactComponent implements OnInit {

  isAlert: boolean = false;

  textAlert: string = 'Successfully';

  contact: Contact;
  office: Office;

  constructor(private contactService: ContactService, private officeService: OfficeService) { }

  ngOnInit() {
    this.contact = {
      ID: null,
      Name: '',
      Email: '',
      Phone: '',
      Subject: '',
      Message: ''
    }
    this.office = {
      ID: -1,
      Name: '',
      Company: '',
      Address1: '',
      Address2: '',
      City: '',
      Nation: '',
      Phone: '',
      Email: '',
      Website: '',
      Copyright: '',
      Logo: '',
      Description: ''
    };
    this.getOffice(1);
  }

  getOffice(id) {
    this.officeService.getOfficeByID(id)
      .subscribe(res => this.office = res as Office, error => console.error(error));
  }

  resetForm(form?: NgForm){
    if(form != null)
      form.reset();
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      this.isAlert = true;
      this.textAlert = "Errors";
      return form.errors;
    }
    else {
      this.contactService.sendMail(form.value).toPromise().then(() => {
        this.resetForm(form);
        this.isAlert = true;
        this.textAlert = "Successfully";
        setTimeout(function(){
          this.isAlert = false;
        }.bind(this), 5000);
      }).catch((ex) => {
        console.error('Error sendMail', ex);
      });
    }
  }

}
