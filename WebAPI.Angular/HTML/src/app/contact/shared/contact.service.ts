import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Contact } from './contact.model';
import { Office } from './office.model';

@Injectable()
export class ContactService {

  constructor(private http: Http) { }

  sendMail(contact: Contact)
  {
    var body = JSON.stringify(contact);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({ method:RequestMethod.Post, headers:headerOptions });
    return this.http.post('http://localhost:53655/api/contact', body, requestOptions);
  }

  getOffice()
  {
    return this.http.get('http://localhost:53655/api/contact')
    .map((res: Response) => res.json());
  }
}
