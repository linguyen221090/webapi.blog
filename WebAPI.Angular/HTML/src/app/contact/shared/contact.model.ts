export class Contact {
    public ID: number;
    public Name: string;
    public Phone: string;
    public Email: string;
    public Subject: string;
    public Message: string;
}
