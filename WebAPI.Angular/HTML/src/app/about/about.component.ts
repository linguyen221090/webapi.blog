import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MenuService } from '../default/menu/shared/menu.service';
import { Menu } from '../default/menu/shared/menu.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  providers: [MenuService]
})
export class AboutComponent implements OnInit {

  _menu: Menu;

  constructor(private router: Router, private menuService: MenuService) { }

  currentRoute = this.router.url;

  ngOnInit() {
    this.menuService.getMenuByUrl(this.currentRoute)
    .subscribe(              
      menu => this._menu = <Menu>menu,
      error => console.error(error)
    )
  };

}
