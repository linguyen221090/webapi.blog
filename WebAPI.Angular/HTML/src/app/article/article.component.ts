import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Article } from './shared/article.model';
import { ArticleService } from './shared/article.service';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleService]
})
export class ArticleComponent implements OnInit {

  selectedObject: Article;
  articles: Article[];

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.articleService.getArticles()
    .subscribe(res => this.articles = res as Article[], error => console.error(error));
  }
  
  onSelect(object: Article): void {
    this.selectedObject = object; // to apply proper styles to the selected product
    this.router.navigate(["/bai-viet" + this.selectedObject.Url]);
    window.scrollTo(0, 0);
  }

}
