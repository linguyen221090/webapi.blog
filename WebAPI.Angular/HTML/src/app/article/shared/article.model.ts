export class Article {
    public ID: number;
    public Name: string;
    public Teaser: string;
    public DateRelease: Date;
    public Description: string;
    public Text: string;
    public Url: string;
    public ParentID: number;
    public Level: number;
    public NodeOrder: number;
}
