import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Article } from './article.model';

@Injectable()
export class ArticleService {

  articles: Article[];
  article: Article;
  urlRouting: String = "";

  constructor(private http: Http) { }

  getArticles()
  {
    return this.http.get('http://localhost:53655/api/article')
    .map((data: Response) => data.json());
  }

  getArticlesPaging(index: Number = 0, page: Number = 20, keywords: String = '') {
    this.urlRouting = "/" + index;
    if (page)
      this.urlRouting += "/" + page;
    if (keywords != '')
      this.urlRouting += "/" + keywords;
    return this.http.get('http://localhost:53655/api/article/paging' + this.urlRouting)
      .map((data: Response) => data.json());
  }

  getArticleChildren(id: Number)
  {
    return this.http.get('http://localhost:53655/api/article/parentid/' + id)
    .map((data: Response) => data.json());
  }

  getArticleChildrenByMenuUrl(){

  }

  getArticleByUrl(url: any)
  {
    return this.http.get('http://localhost:53655/api/article/url' + url)
    .map((res: Response) => res.json())
  }

  getArticleByName(name: any)
  {
    return this.http.get('http://localhost:53655/api/article/search/' + name)
    .map((res: Response) => res.json())
  }

  checkIsUrlExists(id: number, url: any) {
    return this.http.get('http://localhost:53655/api/article/exists/' + id + url);
  }

  postArticle(article, token: String) {
    var body = JSON.stringify(article);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/article',
      body,
      requestOptions)
  }

  putArticle(article, token: String) {
    var body = JSON.stringify(article);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/article',
      body,
      requestOptions)
  }

  deleteArticle(id: Number, token: String)
  {
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({ headers:headerOptions });
    return this.http.delete('http://localhost:53655/api/article/' + id, requestOptions);
  }


}
