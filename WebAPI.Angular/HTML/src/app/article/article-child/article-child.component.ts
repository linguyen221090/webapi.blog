import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, CanLoad } from '@angular/router';

import { Article } from '../shared/article.model';
import { ArticleService } from '../shared/article.service';

import { Menu } from '../../default/menu/shared/menu.model';
import { MenuService } from '../../default/menu/shared/menu.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-article-child',
  templateUrl: './article-child.component.html',
  styleUrls: ['./article-child.component.css'],
  providers: [ArticleService, MenuService]
})
export class ArticleChildComponent implements OnInit {

  selectedObject: Article;
  articles: Article[];
  menuUrl: string;
  currentMenu: Menu;
  _menu: Menu;

  constructor(private menuService: MenuService, private articleService: ArticleService, private router: Router, private route: ActivatedRoute) {
    this.route.params.subscribe(
      params => this.menuUrl = "/" + params['url']
    );
  }

  currentRoute = this.router.url;

  idtest: number;

  ngOnInit() {
    console.log(this.idtest++);
    console.log(this.currentRoute);
    console.log(this.menuUrl);
    this.menuService.getMenuByUrl(this.menuUrl)
      .toPromise()
      .then(res => {
        this._menu = res as Menu;
        console.log(this._menu.ID);
        this.articleService.getArticleChildren(this._menu.ID)
        .subscribe(res => this.articles = res as Article[], error => console.error(error));
      })
      .catch(e => console.error('An error occurred', e));
  }

  canLoad() {
    console.log(this.idtest++);
  }

  bodyChanged()
  {
    console.log(this.idtest++);
  }

  onSelect(object: Article): void {
    this.selectedObject = object; // to apply proper styles to the selected product
    this.router.navigate(["/bai-viet" + this.selectedObject.Url]);
    window.scrollTo(0, 0);
  }

}
