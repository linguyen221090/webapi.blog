import { Component, OnInit } from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import { Article } from '../shared/article.model';
import { ArticleService } from '../shared/article.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css'],
  providers: [ArticleService]
})
export class ArticleDetailComponent implements OnInit {

  articleUrl: String;
  article: Article;
  articles: Article[];

  constructor(private route: ActivatedRoute, private articleService: ArticleService) {
    this.route.params.subscribe(
      params => this.articleUrl = "/" + params['url']
  );
   }

  ngOnInit() {
    this.getArticleDetailByUrl(this.articleUrl);
  }

  getArticleDetailByUrl(url: String)
  {
    this.articleService.getArticleByUrl(url)
    .subscribe(res  => this.article = <Article>res,
     error => console.error(error));
  }

}
