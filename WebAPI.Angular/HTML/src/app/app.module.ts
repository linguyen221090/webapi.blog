import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; // <-- import the module HTTP
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';// <-- import the module Form
import { RouterModule, Routes, ChildrenOutletContexts } from '@angular/router';// <-- import the module Router
import { CKEditorModule } from 'ngx-ckeditor';// <-- import the module CKEditor
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';// <-- import the module Bootstrap Angular
import {NgxPaginationModule} from 'ngx-pagination';// <-- import the module Pagination
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';// <!-- import animations module
import { ToastModule } from 'ng2-toastr/ng2-toastr';// <!-- import toastr module
import {MatRadioModule, MatButtonModule, MatCheckboxModule, MatListModule, MatPaginatorModule, MatSortModule, MatTableModule,MatFormFieldModule,MatInputModule } from '@angular/material';// <!-- import materials
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule,MatSelectModule} from '@angular/material';


import { FilterPipe } from './filter.pipe';
import { FilterMenuRoot, FilterMenu } from './default/menu/shared/filter-parentid-menu.pipe';
import { AppComponent } from './app.component';
import { RolesComponent } from './admin/roles/roles.component';
import { UsersComponent } from './admin/users/users.component';
import { LoginComponent } from './admin/login/login.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginService } from './admin/login/shared/login.service';
import { AuthGuard } from '../app/admin/auth/auth-guard.service';
import { IndexComponent } from './default/index/index.component';
import { MenuTopComponent } from './default/menu/menu-top/menu-top.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ArticleComponent } from './article/article.component';
import { ArticleDetailComponent } from './article/article-detail/article-detail.component';
import { SlideComponent } from './default/slide/slide.component';
import { CategoryComponent } from './default/category/category.component';
import { SearchComponent } from './default/search/search.component';
import { ArticleChildComponent } from './article/article-child/article-child.component';
import { DefaultComponent } from './admin/default/default.component';
import { MenuLeftComponent } from './admin/default/menu-left/menu-left.component';
import { HeaderComponent } from './admin/default/header/header.component';
import { MenuComponent } from './admin/menu/menu.component';
import { ArticleAdminComponent } from './admin/article/article.component';
import { OfficeComponent } from './admin/office/office.component';
import { ArticleRequestComponent } from './admin/articlerequest/articlerequest.component';
import { PaymentComponent } from './admin/payment/payment.component';

const appRoutes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'gioi-thieu', component: AboutComponent },
  { path: 'lien-he', component: ContactComponent },
  { path: 'bai-viet', component: ArticleComponent },
  { path: 'bai-viet/:url', component: ArticleDetailComponent },
  { path: 'chu-de/:url', component: ArticleChildComponent },
  { path: 'login', component: LoginComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: 'request', component: ArticleRequestComponent, canActivate: [AuthGuard] },
  { path: 'payment', component: PaymentComponent, canActivate: [AuthGuard] },
  { path: 'role', component: RolesComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: DefaultComponent, canActivate: [AuthGuard] },
  { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },
  { path: 'article', component: ArticleAdminComponent, canActivate: [AuthGuard] },
  { path: 'office', component: OfficeComponent, canActivate: [AuthGuard] },
  { path: 'tag', component: ArticleAdminComponent, canActivate: [AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    RolesComponent,
    PageNotFoundComponent,
    UsersComponent,
    LoginComponent,
    IndexComponent,
    MenuTopComponent,
    AboutComponent,
    ContactComponent,
    ArticleComponent,
    ArticleDetailComponent,
    SlideComponent,
    CategoryComponent,
    SearchComponent,
    ArticleChildComponent,
    DefaultComponent,
    MenuLeftComponent,
    HeaderComponent,
    MenuComponent,
    ArticleAdminComponent,
    FilterPipe,
    FilterMenuRoot,
    FilterMenu,
    OfficeComponent,
    ArticleRequestComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,// <-- include it in your app module
    HttpModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CKEditorModule,
    BrowserAnimationsModule,// <!-- required animations module
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatIconModule,
    MatGridListModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),
    NgbModule.forRoot(),
    ToastModule.forRoot()// <!-- ToastrModule added
  ],
  providers: [LoginService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
