import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';

import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from './shared/user.model';
import { UserService } from './shared/user.service';
import { RoleService } from '../roles/shared/role.service';
import { JsonResult } from '../../shared/json-result.model'
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Role } from '../roles/shared/role.model';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers:[UserService, RoleService]
})
export class UsersComponent implements OnInit {

  user: User;

  users: User[];

  nameSubmit = "";

  formUser = "Đăng ký";

  inforUser = "Information User";

  boolSelect: boolean = true;

  roleName = "";
  
  errorUserRole = false;

  token = localStorage.getItem('userToken');

  displayedColumns = ['id', 'name', 'phone', 'role', 'action'];
  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService, private roleService: RoleService, private router: Router, 
    private toastr: ToastsManager, private _vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this.getUsers();
    this.roleService.getRoles();
    this.resetForm();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getUsers(){
    this.userService.getUsers()
    .subscribe(res => {
      this.users = res as User[];
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.error(error));
  }

  onEdit(user: User) {
    this.user = Object.assign({}, user);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
  }

  setUserRoleID(id: any, form: NgForm): void {
    
    }

    testForm(form: NgForm)
    {
      if(form.value.userroleid == -1)
      {
        console.log(form.value.userroleid);
        return form.invalid;
      }
    }

    onSubmit(form: NgForm) {
      if(form.value.userroleid == -1)
      {
        return this.errorUserRole = true;
      }
      if (form.value.ID == null || form.value.ID == -1) {
        form.value.ID = -1;
        this.userService.postUser(form.value).toPromise().then(() => {
          this.resetForm(form);
          this.getUsers();
        });
      }
      else {
        this.userService.putUser(form.value.ID, form.value).toPromise().then(() => {
          this.resetForm(form);
          this.getUsers();
        });
      }
    }

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
      this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
      this.errorUserRole = false;
      this.user = new User();
  }

  onDelete(row: any, form?: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.userService.deleteUser(row.ID).toPromise().then(() => {
        this.getUsers();
        this.resetForm(form);
      });
    }
  }

  showInfo(user: any){
    alert(user.Name);
  }

  get diagnostic() { return JSON.stringify(this.user); }

}
