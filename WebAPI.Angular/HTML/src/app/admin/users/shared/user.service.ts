import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions, RequestMethod, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from './user.model';

@Injectable()
export class UserService {

  user: User;

  constructor(private http: Http) { }

  getUsers(){
    return this.http.get("http://localhost:53655/api/user")
    .map((data:Response) => data.json());
  }

  putUser(id, user) {
    var body = JSON.stringify(user);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/user/' + id,
      body,
      requestOptions)
  }

  postUser(user: User)
  {
    var body = JSON.stringify(user);
    var headerOptions = new Headers({ 'Content-Type':'application/json' });
    var requestOptions = new RequestOptions({ method:RequestMethod.Post, headers:headerOptions });
    return this.http.post('http://localhost:53655/api/user',
    body,
    requestOptions)
  }

  deleteUser(id: number)
  {
    return this.http.delete('http://localhost:53655/api/user/' + id);
  }

}
