import { Injectable } from '@angular/core';

import { Chat } from './chat.model';

declare var $: any;
@Injectable()
export class ChatService {

  connection: any;
  proxy: any;
  ulr: any;
  chat: Chat;
  text: string;

  constructor() {

  }

  public startConnection(): void {
    this.connection = $.hubConnection('http://localhost:53655/signalr');
    this.connection.logging = true;
    this.proxy = this.connection.createHubProxy('ChatHub');

    this.connection.start().done((data: any) => {
      console.log('Connected to Processing Hub');
      this.sendHub();
    }).catch((error: any) => {
      console.log('Hub error -> ' + error);
    });

    this.proxy.on("hello", () => {
      alert("ok");
    });

    this.proxy.on("broadcastMessage", (name: string, message: string) => {
      this.chat = {
        Name: name,
        Message: message
      }
      alert(this.chat.Name);
    });

  }

  public helloHub(): void {
    this.proxy.invoke('hello')
      .catch((error: any) => {
        console.log('SendMessage error -> ' + error);
      });
  }

  public sendHub(): void {
    this.proxy.invoke('send', "Li Nguyen", "hi guys")
      .catch((error: any) => {
        console.log('SendMessage error -> ' + error);
      });
  }
}
