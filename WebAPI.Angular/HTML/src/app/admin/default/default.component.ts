import { Component, OnInit } from '@angular/core';
import { Chat } from './shared/chat.model';

declare var $: any;
@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  connection: any;
  proxy: any;
  ulr: any;
  chat: Chat;
  abc: String = "abc";

  constructor() {
    this.startConnection();
  }
  
  ngOnInit() {
    this.chat = {
      Name: '',
      Message: ''
    }
    this.startConnection();
  }

  public startConnection(): void {
    this.connection = $.hubConnection('http://localhost:53655/signalr');
    this.connection.logging = true;
    this.proxy = this.connection.createHubProxy('ChatHub');

    this.connection.start().done((data: any) => {
      console.log('Connected to Processing Hub');
      this.sendHub();
    }).catch((error: any) => {
      console.log('Hub error -> ' + error);
    });

    this.proxy.on("hello", () => {
      alert("ok");
    });

    this.proxy.on("broadcastMessage", (name, message) => {
      this.chat = {
        Name: name,
        Message: message
      }
    });

  }

  public helloHub(): void {
    this.proxy.invoke('hello')
      .catch((error: any) => {
        console.log('SendMessage error -> ' + error);
      });
  }

  public sendHub(): void {
    this.proxy.invoke('send','a','c').done(() => {
      this.abc = "aaa";
    })
      .catch((error: any) => {
        console.log('SendMessage error -> ' + error);
      });
  }
}
