import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  token = localStorage.getItem('userToken');

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signOut(){
    if(this.token != null && this.token != '')
      localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }

}
