import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ArticleService } from '../../article/shared/article.service';
import { Article } from '../../article/shared/article.model';
import { Menu } from '../../default/menu/shared/menu.model';
import { MenuService } from '../../default/menu/shared/menu.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleService, MenuService]
})
export class ArticleAdminComponent implements OnInit {

  article: Article;
  articles: Article[];
  menuOfArticle: Menu[];


  nameSubmit: String;
  closeResult: String;
  searchText: String;
  token = localStorage.getItem("userToken");
  modalReference: NgbModalRef;
  hasUrlExists: boolean;

  constructor(private articleService: ArticleService, private menuService: MenuService,
    private modalService: NgbModal, private router: Router, private toastr: ToastsManager, private _vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this.getList();//get articles
    setTimeout(() => this.toastr.success('Data Loaded Sucessfully!', 'Success'))
  }

  //Open popup to add new
  add(content) {
    this.resetForm();
    this.open(content);
    this.getMenuArticle();
  }

  //Load data to popup to edit
  showForEdit(article: Article, content) {
    this.article = Object.assign({}, article);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
    this.open(content);
    this.checkIsUrlExists(this.article.ID, this.article.Url);
    this.getMenuArticle();
  }

  getMenuArticle() {
    this.menuService.getMenuByLevelParent(2, 3)
      .subscribe(data => {
        this.menuOfArticle = data as Menu[];
      })
  }

  //handle submit form with add or update object
  onSubmit(form: NgForm, content) {
    if (form.invalid) {//check valid form
      this.toastr.warning('Đường dẫn tồn tại!', 'CẢNH BÁO');
      return form.errors;
    }
    if (this.token == null || this.token == "") {//check token
      return this.router.navigate["/login"];
    }
    this.checkIsUrlExists(this.article.ID, this.article.Url);//check url exists
    if (this.hasUrlExists == true) {//result of check url exists
      return form.errors;
    }
    if (this.article.ID > 0) {
      this.articleService.putArticle(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.getList();
        this.JoinAndClose();
      })
        .catch(e => console.error('An error occurred', e));;
    }
    else {
      form.value.ID = -1;
      this.articleService.postArticle(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.getList();
        this.JoinAndClose();
      })
        .catch(e => console.error('An error occurred', e));;
    }
  }

  //check url exists then return true or false
  checkIsUrlExists(id, url: any) {
    this.articleService.checkIsUrlExists(id, url)
      .subscribe(res => {
        if (res.text() == 'true')//has value
        {
          this.toastr.warning('Đường dẫn tồn tại!', 'CẢNH BÁO');
          this.hasUrlExists = true;
        }
        else {
          this.hasUrlExists = false;
        }
      });
  }

  //get list of article
  getList() {
    this.articleService.getArticlesPaging()
      .subscribe(res => this.articles = res as Article[]
        , errors => console.error(errors));
  }

  //map name to url then check exists
  onKey(event: any) {
    //map name to url
    setTimeout(function () {
      if (this.article.Url.length < 75)
        this.article.Url = "/" + this.formatUrlVN(this.article.Name);
    }.bind(this), 500);

    //check url not exists
    setTimeout(function () {
      this.checkIsUrlExists(this.article.ID, this.article.Url);
    }.bind(this), 1000);
  }

  //check url exists
  onKeyUrl(event: any) {
    setTimeout(function () {
      if (this.article.Url.length < 75)
        this.article.Url = "/" + this.formatUrlVN(this.article.Url);
    }.bind(this), 500);
    //check url not exists
    setTimeout(function () {
      this.checkIsUrlExists(this.article.ID, this.article.Url);
    }.bind(this), 1000);
  }

  //search
  onKeySearch(event: any) {
    setTimeout(function () {
      this.articleService.getArticlesPaging(0, 20, this.searchText)
        .subscribe(res => this.articles = res as Article[], error => console.error(error));
    }.bind(this), 2000)
  }

  //Reset form to defaul value
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
    this.article = {
      ID: -1,
      Name: '',
      Url: '/',
      Teaser: '',
      DateRelease: new Date(2000, 1, 1, 0, 0, 0, 0),
      Description: '',
      Text: '',
      ParentID: -1,
      Level: -1,
      NodeOrder: -1
    }
  }

  //delete object
  onDelete(id: number, form?: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.articleService.deleteArticle(id, this.token)
        .toPromise()
        .then(() => {
          this.getList();
          this.resetForm(form);
        })
        .catch(e => console.error('An error occurred', e));
    }
  }

  //format url friendly
  formatUrlVN(alias: any) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    str = str.replace(/ /g, "-");
    return str;
  }

  //#region bootstrap
  open(content) {
    this.modalReference = this.modalService.open(content);
    console.log(this.modalReference.result);
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  JoinAndClose() {
    this.modalReference.close();
  }
  //#endregion

  //Show current data on page
  get diagnostic() { return JSON.stringify(this.article); }

}
