import {Component, OnInit, ViewContainerRef, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';

import { Router } from '@angular/router';
import { JsonResult } from '../../shared/json-result.model'
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { Payment } from './shared/payment.model';
import { PaymentService } from './shared/payment.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
  providers: [PaymentService]
})
export class PaymentComponent implements OnInit {

  private payments: Payment[];
  private payment: Payment;

  UIAction: number = 0;

  status: Status[];
  statusSelected: number;
  
  displayedColumns = ['id', 'note', 'status', 'name', 'action'];
  dataSource: MatTableDataSource<Payment>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private paymentService: PaymentService, private router: Router, 
    private toastr: ToastsManager, private _vcr: ViewContainerRef)
  {
    this.toastr.setRootViewContainerRef(_vcr);
  }

  getList(){
    this.paymentService.getPayments().subscribe(res=>{
      this.payments = res as Payment[];

      const paymetnList: Payment[] = [];

      this.payments.forEach(item=>{
        paymetnList.push(item);
      });

      this.dataSource = new MatTableDataSource(paymetnList);

      console.log(this.payments);
    }, error => console.error(error));
  }

  ngOnInit() {
    this.getList();
    this.status = statusList;
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  showInfo(item:any){
    this.payment = this.payments.find(x=>x.ID == item);
    alert(item);
  }

  listAction(){
    this.UIAction = 0;
  }

  addAction(content:any){
    this.UIAction = 1;
  }

  //Show current data on page
  get diagnostic() { return JSON.stringify(this.payment); }

}

const statusList: Status[] = [
  { ID: 0, Name: 'No paid' },
  { ID: 1, Name: 'Paid' },
  { ID: 2, Name: 'Refund' }
];

export interface Status{
  ID: number,
  Name: string
}
