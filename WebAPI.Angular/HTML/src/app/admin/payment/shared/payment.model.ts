import { User } from "../../users/shared/user.model";

export class Payment {
    ID: Number;
    Note: string;
    Status: Number;
    StatusEnum: Number;
    File: string;
    PMUserID: number;
    User: User;
}
