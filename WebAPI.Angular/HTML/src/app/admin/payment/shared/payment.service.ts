import { Injectable } from '@angular/core';
import { Http, Response, RequestMethod, RequestOptions, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PaymentService {

  constructor(private http: Http) { }

  getPayments() {
    return this.http.get("http://localhost:53655/api/payment")
    .map((data:Response) => data.json());
  }

}
