import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from '../../users/shared/user.model';

@Injectable()
export class LoginService {

  user: User;

  constructor(private httpClient: HttpClient) { }

  userAuthentication(userName, password) {
    console.log(userName);
    console.log(password);
    var data = "username=" + userName + "&password=" + password + "&grant_type=password";
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.httpClient.post('http://localhost:53655/token', data, { headers: reqHeader } );
  }

}
