import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { LoginService } from './shared/login.service';
import { User } from '../users/shared/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router : Router) { }

  user: User

  ngOnInit() {
    this.loginService.user = new User();
  }

  onSubmit(form: NgForm) {
    if(form.invalid)
    {
      return form.errors;
    }
    this.loginService.userAuthentication(this.loginService.user.UserName ,this.loginService.user.UserPassword).subscribe((data : any)=>{
      localStorage.setItem('userToken' , data.access_token);
      this.router.navigate(['/admin']);
    },
    (err : HttpErrorResponse)=>{
      console.log("error");
    });
  }

  get diagnostic() { return JSON.stringify(this.loginService.user); }

}
