import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MenuService } from '../../default/menu/shared/menu.service';
import { Menu } from '../../default/menu/shared/menu.model';
import { JsonResult } from '../../shared/json-result.model'
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [MenuService]
})
export class MenuComponent implements OnInit {

  constructor(private menuService: MenuService, private modalService: NgbModal, private router: Router, 
    private toastr: ToastsManager, private _vcr: ViewContainerRef)
  {
    this.toastr.setRootViewContainerRef(_vcr);
  }

  //#region Default
  menu: Menu;
  menuList: Menu[];

  nameSubmit: String = '';
  closeResult: String;
  searchText: String = '';
  token = localStorage.getItem("userToken");
  modalReference: NgbModalRef;
  errorsGeneric;
  jsonResult: JsonResult;
  //#endregion

  hasUrlExists: boolean;
  menuParent: Number;

  //load first
  ngOnInit() {
    this.getList();
    this.resetForm();
    setTimeout(() => this.toastr.success('Data Loaded Sucessfully!', 'Success'))
  }

  //Open popup to add new
  add(content) {
    this.resetForm();
    this.open(content);
  }

  //Load data to popup to edit
  showForEdit(menu: Menu, content) {
    this.menu = Object.assign({}, menu);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
    this.open(content);
    // this.checkIsUrlExists(this.menu.ID, this.menu.Url);
    this.menuParent = this.menu.ParentID;
  }

  //check url exists then return true or false
  checkIsUrlExists(id, url: any) {
    this.menuService.checkIsUrlExists(id, url)
      .subscribe(res => {
        if (res.text() == 'true')//has value
        {
          this.errorsGeneric = "Url is exists.";
          this.hasUrlExists = true;
        }
        else {
          this.errorsGeneric = null;
          this.hasUrlExists = false;
        }
      });
  }

  //handle submit form with add or update object
  onSubmit(form: NgForm, content) {
    console.log(form.value);
    if (form.invalid) {//check valid form
      return form.errors;
    }
    if (this.token == null || this.token == "") {//check token
      return this.router.navigate["/login"];
    }
    this.checkIsUrlExists(this.menu.ID, this.menu.Url);//check url exists
    if (this.hasUrlExists == true) {//result of check url exists
      this.toastr.error(this.errorsGeneric)
      return form.errors;
    }
    if (form.value.ID != null && form.value.ID > 0) {
      if (this.menuParent == this.menu.ParentID) {//update without changed parent
        this.menuService.putMenu(form.value, this.token).toPromise().then(x => {
          this.jsonResult = x.json();
          if(this.jsonResult.ErrorCode == 200)
          {
            this.toastr.success(this.jsonResult.Message.toString(),"Success");
            this.resetForm(form);
            this.getList();
            this.JoinAndClose();
          }
        })
          .catch(e => console.error('An error occurred', e));;
      }
      else {
        this.menuService.putMenuAndParent(form.value, this.token).toPromise().then(x => {
          this.jsonResult = x.json();
          if(this.jsonResult.ErrorCode == 200)
          {
            this.toastr.success(this.jsonResult.Message.toString(),"Success");
            this.resetForm(form);
            this.getList();
            this.JoinAndClose();
          }
        })
          .catch(e => console.error('An error occurred', e));;
      }
    }
    else {
      form.value.ID = -1;
      this.menuService.postMenu(form.value, this.token).toPromise().then(x => {
        if(this.jsonResult.ErrorCode == 200)
        {
          this.toastr.success(this.jsonResult.Message.toString(),"Success");
          this.resetForm();
          this.getList();
          this.JoinAndClose();
        }
      })
        .catch(e => console.error('An error occurred', e));;
    }
  }

  //format url friendly
  formatUrlVN(alias: any) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    str = str.replace(/ /g, "-");
    return str;
  }

  //map name to url then check exists
  onKey(event: any) {
    //map name to url
    setTimeout(function () {
      if (this.menu.Url.length < 75)
        this.menu.Url = "/" + this.formatUrlVN(this.menu.Name);
    }.bind(this), 500);

    //check url not exists
    setTimeout(function () {
      this.checkIsUrlExists(this.menu.ID, this.menu.Url);
    }.bind(this), 1000);
  }

  //check url exists
  onKeyUrl(event: any) {
    setTimeout(function () {
      if (this.menu.Url.length < 75)
        this.menu.Url = "/" + this.formatUrlVN(this.menu.Url);
    }.bind(this), 500);

    //check url not exists
    setTimeout(function () {
      this.checkIsUrlExists(this.menu.ID, this.menu.Url);
    }.bind(this), 1000);
  }

  sumMenusByParent(parentid: number){
    if(this.menuList.filter(m=>m.ParentID == parentid) != null)
      return this.menuList.filter(m=>m.ParentID == parentid).length;
    return 0;
  }

  sumMenusByParent1(){
    return this.menuList.filter(m=>m.ParentID == 3).length;
  }

  hasMenusByParent(parentid: number){
    return "aaa";
  }

  //search
  onKeySearch(event: any) {
    setTimeout(function () {
      this.menuService.getMenusPaging(0, 20, this.searchText)
        .subscribe(res => this.menuList = res as Menu[], error => console.error(error));
    }.bind(this), 2000)
  }

  //Get list data
  getList() {
    this.menuService.getMenusPaging()
      .subscribe(res => {
        this.menuList = res as Menu[];
      }, error => console.error(error));
  }

  //Reset form to defaul value
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
    this.menu = {
      ID: -1,
      Name: '',
      Group: -1,
      Icon: '',
      Url: '/',
      Level: 0,
      ParentID: 0,
      NodeOrder: 0,
      Text: ''
    }
  }

  //delete object
  onDelete(id: number, form?: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.menuService.deleteMenu(id, this.token)
        .toPromise()
        .then(() => {
          this.getList();
          this.resetForm(form);
        })
        .catch(e => console.error('An error occurred', e));
    }
  }

  //Change node order in list
  putNodeOrder(menu: Menu, e: boolean) {
    this.menuService.putNodeOrder(menu, e, this.token).toPromise().then(() => {
      this.getList();
    })
      .catch(e => console.error('An error occurred', e));;
  }


  //#region bootstrap
  open(content) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  JoinAndClose() {
    this.modalReference.close();
  }
  //#endregion

  //Show current data on page
  get diagnostic() { return JSON.stringify(this.menu); }

}
