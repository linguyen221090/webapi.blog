import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { Office } from './shared/office.model';
import { OfficeService } from './shared/office.service';

@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.css'],
  providers: [OfficeService]
})
export class OfficeComponent implements OnInit {

  nameSubmit: String;
  office: Office;
  token = localStorage.getItem("userToken");
  toastr1;

  constructor(private officeService: OfficeService, private router: Router, private toastr:ToastsManager, private _vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(_vcr);
  }

  ngOnInit() {
    this.office = {
      ID: -1,
      Name: '',
      Company: '',
      Address1: '',
      Address2: '',
      City: '',
      Nation: '',
      Phone: '',
      Email: '',
      Website: '',
      Copyright: '',
      Logo: '',
      Description: ''
    };
    this.getOffice(1);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
    setTimeout(() => this.toastr.success('Tải dự liệu thành công.','THÔNG BÁO'))
  }

  getOffice(id) {
    this.officeService.getOfficeByID(id)
      .subscribe(res => this.office = res as Office, error => console.error(error));
  }

  //handle submit form with add or update object
  onSubmit(form: NgForm, content) {
    if (form.invalid) {//check valid form
      this.toastr.warning('Vui lòng nhập đầy đủ thông tin yêu cầu.','Cảnh báo');
      return form.errors;
    }
    if (this.token == null || this.token == "") {//check token
      return this.router.navigate["/login"];
    }
    if (this.office.ID > 0) {
      this.officeService.putOffice(form.value, this.token).toPromise().then(() => {
        this.toastr.success('Cập nhật dữ liệu thành công!','Thông báo');
      })
        .catch(e => this.toastr.error(e,'Lỗi'));;
    }
  }

  //Show current data on page
  get diagnostic() { return JSON.stringify(this.office); }

}
