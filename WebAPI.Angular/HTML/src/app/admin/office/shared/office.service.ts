import { Injectable } from '@angular/core';

import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Office } from './office.model';

@Injectable()
export class OfficeService {

  constructor(private http: Http) { }

  getOfficeByID(id){
    return this.http.get('http://localhost:53655/api/office/' + id)
    .map((data: Response) => data.json());
  }

  putOffice(article, token: String) {
    var body = JSON.stringify(article);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/office',
      body,
      requestOptions)
  }

}
