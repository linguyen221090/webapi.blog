export class Office {
    public ID: Number;
    public Name: String;
    public Company: String;
    public Address1: String;
    public Address2: String;
    public City: String;
    public Nation: String;
    public Phone: String;
    public Email: String;
    public Website: String;
    public Copyright: String;
    public Logo: String;
    public Description: String;
}
