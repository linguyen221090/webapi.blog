import {Component, OnInit, ViewContainerRef, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';

import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { JsonResult } from '../../shared/json-result.model'
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ArticleRequest } from './shared/articlerequest.model';
import { ArticleRequestService } from './shared/articlerequest.service';
import { forEach } from '@angular/router/src/utils/collection';
import { User } from '../users/shared/user.model';
import { UserService } from '../users/shared/user.service';

@Component({
  selector: 'app-articlerequest',
  templateUrl: './articlerequest.component.html',
  styleUrls: ['./articlerequest.component.css'],
  providers: [ArticleRequestService, UserService]
})
export class ArticleRequestComponent implements OnInit {

  articleRequest: ArticleRequest;
  articleRequests: ArticleRequest[];

  user: User;
  users: User[];
  
  UIAction: number = 0;
  nameSubmit: String;
  token = localStorage.getItem("userToken");

  displayedColumns = ['id', 'topic', 'number', 'website', 'user' ,'action'];
  dataSource: MatTableDataSource<ArticleRequest>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private articleRequestService: ArticleRequestService, private userService: UserService, private router: Router, 
    private toastr: ToastsManager, private _vcr: ViewContainerRef)
  {
    this.toastr.setRootViewContainerRef(_vcr);
    // Assign the data to the data source for the table to render
  }

  getUser(){
    this.userService.getUsers()
    .subscribe(res => {
      this.users = res as User[];
    }, error => console.error(error));
  }

  getList(){
    this.articleRequestService.getArticleRequests().subscribe(res=>{
      this.articleRequests = res as ArticleRequest[];
      this.dataSource = new MatTableDataSource(this.articleRequests);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      setTimeout(() => this.toastr.success('Data Loaded Sucessfully!', 'Success'))
    }, error => console.error(error));
  }

  ngOnInit() {
    this.resetForm();
    this.getList();
    this.getUser();
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showInfo(item:any){
    this.articleRequest = this.articleRequests.find(x=>x.ID == item);
    alert(item);
  }


  //handle submit form with add or update object
  onSubmit(form: NgForm, content) {
    if (this.token == null || this.token == "") {//check token
      return this.router.navigate["/login"];
    }
    if (this.articleRequest.ID > 0) {
      this.articleRequestService.putArticleRequest(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.UIAction = 0;
        this.getList();
      })
        .catch(e => console.error('An error occurred', e));;
    }
    else {
      form.value.ID = -1;
      this.articleRequestService.postArticleRequest(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.UIAction = 0;
        this.getList();
      })
        .catch(e => console.error('An error occurred', e));;
    }
  }

  onEdit(articleRequest: ArticleRequest){
    this.articleRequest = Object.assign({}, articleRequest);
    this.UIAction = 1;
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Edit";
    console.log(this.articleRequest.Topic)
  }

  onDelete(row:any,event:any){
    if(confirm("Are you sure to delete "+row.ID)) {
      
    }
  }

  //Reset form to defaul value
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
    this.articleRequest = new ArticleRequest;
  }

  listAction(){
    this.UIAction = 0;
  }

  addAction(content:any){
    this.UIAction = 1;
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
  }

  //Show current data on page
  get diagnostic() { return JSON.stringify(this.articleRequest); }

}
