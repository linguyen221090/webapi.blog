import { User } from "../../users/shared/user.model";
import { Payment } from "../../payment/shared/payment.model";

export class ArticleRequest {
    ID: number;
    Topic: string;
    Number: number;
    DateFrom: Date;
    DateTo: Date;
    File: string;
    Website: string;
    Note: string;
    ARUserID: number;
    ARPaymentID: number;
    
    User: User;
    Payment: Payment;
}
