import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions, RequestMethod, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ArticleRequest } from './articlerequest.model';

@Injectable()
export class ArticleRequestService {

  constructor(private http: Http) { }

  getArticleRequests(){
    return this.http.get("http://localhost:53655/api/articlerequest")
    .map((data:Response) => data.json());
  }

  postArticleRequest(articleRequest, token: String) {
    var body = JSON.stringify(articleRequest);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/articlerequest',
      body,
      requestOptions)
  }

  putArticleRequest(articleRequest, token: String) {
    var body = JSON.stringify(articleRequest);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/articlerequest',
      body,
      requestOptions)
  }

  deleteArticleRequest(id: Number, token: String)
  {
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({ headers:headerOptions });
    return this.http.delete('http://localhost:53655/api/articlerequest/' + id, requestOptions);
  }
}
