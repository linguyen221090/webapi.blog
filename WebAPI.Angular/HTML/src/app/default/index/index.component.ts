import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ArticleService } from '../../article/shared/article.service';
import { Article } from '../../article/shared/article.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ArticleService]
})
export class IndexComponent implements OnInit {

  articles: Article[];

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.articleService.getArticles()
    .subscribe(res => this.articles = res as Article[], error => console.error(error));
  }
  
  onSelect(object: Article): void {
    // this.selectedObject = object; // to apply proper styles to the selected product
    this.router.navigate(["/bai-viet" + object.Url]);
    window.scrollTo(0, 0);
  }

}
