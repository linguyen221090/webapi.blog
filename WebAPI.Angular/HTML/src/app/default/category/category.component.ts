import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MenuService } from '../menu/shared/menu.service';
import { Menu} from '../menu/shared/menu.model';

import { ArticleDetailComponent } from '../../article/article-detail/article-detail.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [MenuService]
})
export class CategoryComponent implements OnInit {

  menuCategory: Menu[];
  selectedObject: Menu;

  @ViewChild(ArticleDetailComponent) private articleDetailComponent:ArticleDetailComponent;

  constructor(private menuService: MenuService, private router: Router) { }

  ngOnInit() {
    this.menuService.getMenuByLevelParent(2, 3)
    .subscribe(res => this.menuCategory = res as Menu[], error => console.error(error));
  }

  onSelect(object: Menu): void {
    this.selectedObject = object; // to apply proper styles to the selected product
    this.router.navigate(["/chu-de" + this.selectedObject.Url]);
    window.scrollTo(0, 0);
  }

  ngAfterViewInit() {
    // this.articleDetailComponent.getArticleDetailByUrl(this.selectedObject.Url);
  }

}
