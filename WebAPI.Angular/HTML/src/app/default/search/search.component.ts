import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ArticleService } from '../../article/shared/article.service';
import { Article } from '../../article/shared/article.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [ArticleService]
})
export class SearchComponent implements OnInit {

  articles: Article[];
  searchArticle: string = "";
  selectedObject: Article;

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
  }

  onKey(event: any) { // without type info
    if(this.searchArticle == "")
    {
      this.articles = null;
      return;
    }

    setTimeout(function () {
      this.articleService.getArticleByName(this.searchArticle)
      .subscribe(res => this.articles = res as Article[])
    }.bind(this), 1000);

  }
  
  onSelect(object: Article): void {
    this.selectedObject = object; // to apply proper styles to the selected product
    this.router.navigate(["/bai-viet" + this.selectedObject.Url]);
    window.scrollTo(0, 0);
  }

}
