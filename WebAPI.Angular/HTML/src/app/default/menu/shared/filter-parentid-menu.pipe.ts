import { Pipe, PipeTransform } from '@angular/core';
import { Menu } from './menu.model';
import { identifierName } from '@angular/compiler';

@Pipe({
  name: 'FilterMenuRoot'
})

export class FilterMenuRoot implements PipeTransform {
  transform(items: any[], id: number = 0): any[] {

    if (!items) {
        return items;
    }

    return items.filter(item  => item.ParentID == id); 
   }
}

@Pipe({
    name: 'FilterMenu',
    pure: false
  })

export class FilterMenu extends FilterMenuRoot {
    transform(items: any[], id: number): any[] {

        if (!items && !identifierName) {
            return items;
        }
        return items.filter(item  => item.ParentID == id); 
       }
}