export class Menu {
    public ID: Number;
    public Name: String;
    public Icon: String;
    public Group: Number;
    public Url: String;
    public Level: Number;
    public ParentID: Number;
    public NodeOrder: Number;
    public Text: String;
}
