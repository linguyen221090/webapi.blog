import { Injectable } from '@angular/core';
import { Http, Response, RequestMethod, RequestOptions, Headers } from '@angular/http';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Menu } from './menu.model';

@Injectable()
export class MenuService {

  menus: Menu[];
  menu: Menu;
  urlRouting: String = "";

  constructor(private http: Http) { }

  getMenus() {
    this.http.get('http://localhost:53655/api/menu')
      .map((data: Response) => {
        return data.json() as Menu[];
      })
      .toPromise()
      .then(x => {
        this.menus = x;
      })
      .catch(e => console.error('An error occurred', e));
  }

  getMenuByLevelParent(level: number = -1, parentID: number = -1) {
    return this.http.get('http://localhost:53655/api/menu/level/' + level + '/' + parentID)
      .map((data: Response) => data.json());
  }

  getMenuByUrl(url: any) {
    return this.http.get('http://localhost:53655/api/menu/url' + url)
      .map((data: Response) => data.json());
  }

  sumMenusByParent(parentid: number) {
    return this.http.get('http://localhost:53655/api/menu/sum/' + parentid)
      .map((data: Response) => data.json() as Number);
  }

  checkIsUrlExists(id: number, url: any) {
    return this.http.get('http://localhost:53655/api/menu/exists/' + id + url);
  }

  postMenu(menu, token: String) {
    var body = JSON.stringify(menu);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/menu/',
      body,
      requestOptions)
  }

  putNodeOrder(menu, e: Boolean, token: any) {
    var _event: Boolean;
    if (e == true)
      _event = true;
    if (e == false)
      _event = false;
    console.log(_event);
    var body = JSON.stringify(menu);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/menu/order/' + _event,
      body,
      requestOptions)
  }

  putMenu(menu, token: String) {
    var body = JSON.stringify(menu);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/menu/',
      body,
      requestOptions)
  }

  putMenuAndParent(menu, token: String) {
    var body = JSON.stringify(menu);
    var headerOptions = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'bearer ' + token });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/menu/putChangeParent',
      body,
      requestOptions)
  }

  getMenusPaging(index: Number = 0, page: Number = 20, keywords: String = '') {
    this.urlRouting = "/" + index;
    if (page)
      this.urlRouting += "/" + page;
    if (keywords != '')
      this.urlRouting += "/" + keywords;
    return this.http.get('http://localhost:53655/api/menu/paging' + this.urlRouting)
      .map((data: Response) => data.json());
  }

  deleteMenu(id: Number, token: String)
  {
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({headers:headerOptions});
    return this.http.delete('http://localhost:53655/api/menu/' + id, requestOptions);
  }

}
