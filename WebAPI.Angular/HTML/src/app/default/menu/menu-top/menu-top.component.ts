import { Component, OnInit } from '@angular/core';

import { MenuService } from '../shared/menu.service';
import { Menu } from '../shared/menu.model';

@Component({
  selector: 'app-menu-top',
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.css'],
  providers: [MenuService]
})
export class MenuTopComponent implements OnInit {

  constructor(private menuService: MenuService) { }

  menuTop: Menu[];

  ngOnInit() {
    this.menuService.getMenuByLevelParent(1, 0)
    .subscribe(res => this.menuTop = res as Menu[], error => console.error(error));
  }



}
