import { Component, OnInit } from '@angular/core';

import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {

  isHome: boolean = false;

  constructor(private router: Router) { 
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd ) {
        if(event.url == "/")
          this.isHome = true;
        else
          this.isHome = false;
      }
    });
  }

  ngOnInit() {
    
  }

}
