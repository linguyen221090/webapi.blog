export class JsonResult {
    public ErrorCode: Number;
    public Status: Boolean;
    public Message: String;
    public JsonData: String
}
