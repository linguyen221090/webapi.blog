import { Component, ViewChild } from '@angular/core';
import { SlideComponent } from './default/slide/slide.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() { 
  }

  @ViewChild(SlideComponent) slide: SlideComponent;

  ngAfterViewInit() {
    
  }

  logOut()
  {
    if(localStorage.getItem("userToken") != null && localStorage.getItem("userToken") != "")
    {
      console.log("has value");
    }
    console.log("not value");
    localStorage.setItem("userToken", "");
    return;
  }
}
