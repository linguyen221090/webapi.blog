﻿using Web.Data.Infrastructure;
using Web.Model.Models;

namespace Web.Data.Repositories
{
    public interface IArticleRequestRepository : IRepository<ArticleRequest>
    {

    }

    public class ArticleRequestRepository : RepositoryBase<ArticleRequest>, IArticleRequestRepository
    {
        public ArticleRequestRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
