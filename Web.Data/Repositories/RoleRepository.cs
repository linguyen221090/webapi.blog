﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Data.Infrastructure;
using Web.Model.Models;

namespace Web.Data.Repositories
{
    public interface IRoleRepository: IRepository<Role>
    {
        IEnumerable<Role> GetListByName(string name);
    }

    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        public RoleRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<Role> GetListByName(string name)
        {
            var roles = (from r in DbContext.Roles
                         where r.RoleDisplayName.Contains(name) || r.RoleName.Contains(name)
                         orderby r.ID
                        select r).Distinct();
            if(roles == null || roles.Count() == 0)
            {
                return null;
            }
            return roles;
        }

    }
}
