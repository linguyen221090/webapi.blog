﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;

namespace Web.Data
{
    public class WebDbContext : DbContext
    {
        public WebDbContext() : base("WebAPICodeFirstConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<WebDbContext>(null);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<ArticleRequest> ArticleRequest { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public static WebDbContext Create()
        {
            return new WebDbContext();
        }
    }
}
