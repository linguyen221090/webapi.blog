﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;

namespace Web.Data.Common
{
    public class UserContext
    {
        private WebDbContext _db = new WebDbContext();

        public User ValidLogin(string username, string password)
        {
            var user = (from r in _db.Users
                        where r.UserName.Equals(username) && r.UserPassword.Equals(password)
                        select r).FirstOrDefault();
            if (user == null)
                return null;
            return user;
        }

    }
}
