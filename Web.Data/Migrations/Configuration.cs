namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Web.Model.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Data.WebDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Web.Data.WebDbContext context)
        {
            context.Roles.AddOrUpdate(x => x.ID,
            new Role() { ID = 1, RoleName = "admin", RoleDisplayName = "Administrator" },
            new Role() { ID = 2, RoleName = "editor", RoleDisplayName = "Editor" }
            );

            context.Users.AddOrUpdate(x => x.ID,
            new User() { ID = 1, Name = "Jane Austen", Birthday = null, Email = "", Phone = "", GuiD = Guid.NewGuid(), UserName = "jane", UserPassword = "123", UserRoleID = 1 },
            new User() { ID = 2, Name = "Join Tom", Birthday = null, Email = "", Phone = "", GuiD = Guid.NewGuid(), UserName = "join", UserPassword = "123", UserRoleID = 2 }
            );

            context.Menus.AddOrUpdate(x => x.ID,
            new Menu() { ID = 1, Name = "Trang chủ", Url = "/", ParentID = 0, Level = 1, NodeOrder = 1  },
            new Menu() { ID = 2, Name = "Giới thiệu", Url = "/gioi-thieu", ParentID = 0, Level = 1, NodeOrder = 2, Text = "Đang cập nhật phần giới thiệu ... :)" },
            new Menu() { ID = 3, Name = "Bài viết", Url = "/bai-viet", ParentID = 0, Level = 1, NodeOrder = 3 },
            new Menu() { ID = 5, Name = "Học Angular Việt Hóa", Url = "/hoc-angular-viet-hoa", ParentID = 3, Level = 2, NodeOrder = 1 },
            new Menu() { ID = 6, Name = "Phiên bản Angular", Url = "/phien-ban-angular", ParentID = 3, Level = 2, NodeOrder = 2 },
            new Menu() { ID = 7, Name = "Hỏi đáp Angular", Url = "/hoi-dap-angular", ParentID = 3, Level = 2, NodeOrder = 3 },
            new Menu() { ID = 4, Name = "Liên hệ", Url = "/lien-he", ParentID = 0, Level = 1, NodeOrder = 4 }
            );

            context.Articles.AddOrUpdate(x => x.ID,
            new Article() { ID = 1, Name = "Angular là gì?", Teaser = "https://christianliebel.com/wp-content/uploads/2016/02/Angular2-825x510.png", Description = "Đang cập nhật ...", DateRelease = DateTime.Now, Text = "Đang cập nhật ...", Url = "/bai-viet/hoc-angular-viet-hoa/angular-la-gi", ParentID = 5, Level = 3, NodeOrder = 1 },
            new Article() { ID = 2, Name = "Ưu và nhược điểm của Angular?", Teaser = "https://christianliebel.com/wp-content/uploads/2016/02/Angular2-825x510.png", Description = "Đang cập nhật ...", DateRelease = DateTime.Now, Text = "Đang cập nhật ...", Url = "/uu-va-nhuoc-diem-cua-angular", ParentID = 6, Level = 3, NodeOrder = 1 },
            new Article() { ID = 3, Name = "Tại sao Angular đang là thư viện javascript phổ biến?", Teaser = "https://christianliebel.com/wp-content/uploads/2016/02/Angular2-825x510.png", Description = "Đang cập nhật ...", DateRelease = DateTime.Now, Text = "Đang cập nhật ...", Url = "/tai-sao-angular-dang-la-thu-vien-javascript-pho-bien-nhat", ParentID = 7, Level = 3, NodeOrder = 1 }
            );

            context.Offices.AddOrUpdate(x => x.ID,
            new Office() { ID = 1, Name = "Angular Tutor", Company = "Nash Tech", Address1 = "364 Cong Hoa Street" }
            );

        }
    }
}
