namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateNameOfOfficeAndUser : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Offices", newName: "Office");
            RenameTable(name: "dbo.Roles", newName: "Role");
            RenameTable(name: "dbo.Users", newName: "User");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.User", newName: "Users");
            RenameTable(name: "dbo.Role", newName: "Roles");
            RenameTable(name: "dbo.Office", newName: "Offices");
        }
    }
}
