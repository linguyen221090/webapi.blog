namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLogicPaymentForArticleRequest : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Article", "Payment_ID", "dbo.Payments");
            DropForeignKey("dbo.Article", "User_ID", "dbo.User");
            DropForeignKey("dbo.Payments", "PMUserID", "dbo.User");
            DropIndex("dbo.Payments", new[] { "PMUserID" });
            DropIndex("dbo.Article", new[] { "Payment_ID" });
            DropIndex("dbo.Article", new[] { "User_ID" });
            AddColumn("dbo.ArticleRequests", "PMUserID", c => c.Int());
            CreateIndex("dbo.ArticleRequests", "PMUserID");
            AddForeignKey("dbo.ArticleRequests", "PMUserID", "dbo.User", "ID");
            DropColumn("dbo.Payments", "PMUserID");
            DropColumn("dbo.Article", "Payment_ID");
            DropColumn("dbo.Article", "User_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Article", "User_ID", c => c.Int());
            AddColumn("dbo.Article", "Payment_ID", c => c.Int());
            AddColumn("dbo.Payments", "PMUserID", c => c.Int());
            DropForeignKey("dbo.ArticleRequests", "PMUserID", "dbo.User");
            DropIndex("dbo.ArticleRequests", new[] { "PMUserID" });
            DropColumn("dbo.ArticleRequests", "PMUserID");
            CreateIndex("dbo.Article", "User_ID");
            CreateIndex("dbo.Article", "Payment_ID");
            CreateIndex("dbo.Payments", "PMUserID");
            AddForeignKey("dbo.Payments", "PMUserID", "dbo.User", "ID");
            AddForeignKey("dbo.Article", "User_ID", "dbo.User", "ID");
            AddForeignKey("dbo.Article", "Payment_ID", "dbo.Payments", "ID");
        }
    }
}
