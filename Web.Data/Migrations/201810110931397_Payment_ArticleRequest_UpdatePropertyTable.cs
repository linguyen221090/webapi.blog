namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Payment_ArticleRequest_UpdatePropertyTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArticleRequests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Topic = c.String(nullable: false, maxLength: 500),
                        Number = c.Int(nullable: false),
                        DateFrom = c.DateTime(),
                        DateTo = c.DateTime(),
                        File = c.String(),
                        Website = c.String(),
                        Note = c.String(),
                        ARPaymentID = c.Int(nullable: false),
                        CreatedByUser = c.Int(),
                        ModifiedByUser = c.Int(),
                        WhenCreated = c.DateTime(nullable: false),
                        WhenModified = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Payments", t => t.ARPaymentID, cascadeDelete: true)
                .Index(t => t.ARPaymentID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        Status = c.String(),
                        File = c.String(),
                        PMUserID = c.Int(nullable: false),
                        CreatedByUser = c.Int(),
                        ModifiedByUser = c.Int(),
                        WhenCreated = c.DateTime(nullable: false),
                        WhenModified = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.PMUserID, cascadeDelete: true)
                .Index(t => t.PMUserID);
            
            AddColumn("dbo.Article", "Payment_ID", c => c.Int());
            AddColumn("dbo.Article", "User_ID", c => c.Int());
            AddColumn("dbo.User", "CreatedByUser", c => c.Int());
            AddColumn("dbo.User", "ModifiedByUser", c => c.Int());
            AddColumn("dbo.User", "WhenCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.User", "WhenModified", c => c.DateTime());
            AddColumn("dbo.User", "IsDeleted", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Article", "CreatedByUser", c => c.Int());
            AlterColumn("dbo.Article", "ModifiedByUser", c => c.Int());
            AlterColumn("dbo.Article", "WhenModified", c => c.DateTime());
            AlterColumn("dbo.Menu", "CreatedByUser", c => c.Int());
            AlterColumn("dbo.Menu", "ModifiedByUser", c => c.Int());
            AlterColumn("dbo.Menu", "WhenModified", c => c.DateTime());
            CreateIndex("dbo.Article", "Payment_ID");
            CreateIndex("dbo.Article", "User_ID");
            AddForeignKey("dbo.Article", "Payment_ID", "dbo.Payments", "ID");
            AddForeignKey("dbo.Article", "User_ID", "dbo.User", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ArticleRequests", "ARPaymentID", "dbo.Payments");
            DropForeignKey("dbo.Payments", "PMUserID", "dbo.User");
            DropForeignKey("dbo.Article", "User_ID", "dbo.User");
            DropForeignKey("dbo.Article", "Payment_ID", "dbo.Payments");
            DropIndex("dbo.Article", new[] { "User_ID" });
            DropIndex("dbo.Article", new[] { "Payment_ID" });
            DropIndex("dbo.Payments", new[] { "PMUserID" });
            DropIndex("dbo.ArticleRequests", new[] { "ARPaymentID" });
            AlterColumn("dbo.Menu", "WhenModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Menu", "ModifiedByUser", c => c.Int(nullable: false));
            AlterColumn("dbo.Menu", "CreatedByUser", c => c.Int(nullable: false));
            AlterColumn("dbo.Article", "WhenModified", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Article", "ModifiedByUser", c => c.Int(nullable: false));
            AlterColumn("dbo.Article", "CreatedByUser", c => c.Int(nullable: false));
            DropColumn("dbo.User", "IsDeleted");
            DropColumn("dbo.User", "WhenModified");
            DropColumn("dbo.User", "WhenCreated");
            DropColumn("dbo.User", "ModifiedByUser");
            DropColumn("dbo.User", "CreatedByUser");
            DropColumn("dbo.Article", "User_ID");
            DropColumn("dbo.Article", "Payment_ID");
            DropTable("dbo.Payments");
            DropTable("dbo.ArticleRequests");
        }
    }
}
