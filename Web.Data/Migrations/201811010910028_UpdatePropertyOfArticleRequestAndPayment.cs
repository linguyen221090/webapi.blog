namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePropertyOfArticleRequestAndPayment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ArticleRequests", "ARPaymentID", "dbo.Payments");
            DropForeignKey("dbo.Payments", "PMUserID", "dbo.User");
            DropIndex("dbo.ArticleRequests", new[] { "ARPaymentID" });
            DropIndex("dbo.Payments", new[] { "PMUserID" });
            AlterColumn("dbo.ArticleRequests", "ARPaymentID", c => c.Int());
            AlterColumn("dbo.Payments", "PMUserID", c => c.Int());
            CreateIndex("dbo.ArticleRequests", "ARPaymentID");
            CreateIndex("dbo.Payments", "PMUserID");
            AddForeignKey("dbo.ArticleRequests", "ARPaymentID", "dbo.Payments", "ID");
            AddForeignKey("dbo.Payments", "PMUserID", "dbo.User", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "PMUserID", "dbo.User");
            DropForeignKey("dbo.ArticleRequests", "ARPaymentID", "dbo.Payments");
            DropIndex("dbo.Payments", new[] { "PMUserID" });
            DropIndex("dbo.ArticleRequests", new[] { "ARPaymentID" });
            AlterColumn("dbo.Payments", "PMUserID", c => c.Int(nullable: false));
            AlterColumn("dbo.ArticleRequests", "ARPaymentID", c => c.Int(nullable: false));
            CreateIndex("dbo.Payments", "PMUserID");
            CreateIndex("dbo.ArticleRequests", "ARPaymentID");
            AddForeignKey("dbo.Payments", "PMUserID", "dbo.User", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ArticleRequests", "ARPaymentID", "dbo.Payments", "ID", cascadeDelete: true);
        }
    }
}
