namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateNationOffice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Office", "Nation", c => c.String(maxLength: 100));
            DropColumn("dbo.Office", "National");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Office", "National", c => c.String(maxLength: 100));
            DropColumn("dbo.Office", "Nation");
        }
    }
}
