namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateNameOfUserIDInArticleRequest : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ArticleRequests", name: "PMUserID", newName: "ARUserID");
            RenameIndex(table: "dbo.ArticleRequests", name: "IX_PMUserID", newName: "IX_ARUserID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ArticleRequests", name: "IX_ARUserID", newName: "IX_PMUserID");
            RenameColumn(table: "dbo.ArticleRequests", name: "ARUserID", newName: "PMUserID");
        }
    }
}
