namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_StatusOfPayment : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Payments", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Payments", "Status", c => c.String());
        }
    }
}
