﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;
using Web.Data.Repositories;
using Web.Data.Infrastructure;

namespace Web.Service
{
    public interface IOfficeService
    {
        Office GetByID(int id);
        void Update(Office office);
        bool DoSomething(string value);
        void Save();
    }
    public class OfficeService : IOfficeService
    {
        IOfficeRepository _officeRepository;
        IUnitOfWork _unitOfWork;

        public OfficeService(IOfficeRepository officeRepository, IUnitOfWork unitOfWork)
        {
            this._officeRepository = officeRepository;
            this._unitOfWork = unitOfWork;
        }

        public Office GetByID(int id)
        {
            return _officeRepository.GetSingleByCondition(x => x.ID.Equals(id));
        }

        public void Update(Office office)
        {
            _officeRepository.Update(office);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public bool DoSomething(string value)
        {
            if (Convert.ToString(value) != null)
                return true;
            return false;
        }
    }
}
