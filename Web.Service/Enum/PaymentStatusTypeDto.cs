﻿namespace Web.Service.Enum
{
    public enum PaymentStatusTypeDto
    {
        NoPaid = 0,
        Paid = 1,
        Refund = 2
    }
}
