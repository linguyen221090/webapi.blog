﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;
using Web.Data.Repositories;
using Web.Data.Infrastructure;

namespace Web.Service
{
    public interface IArticleService
    {
        void Add(Article article);
        void Update(Article Article);
        void Delete(Article Article);
        Article GetByID(int id);
        Article GetArticleByUrl(string url);
        IEnumerable<Article> CheckIsUrlExists(int id, string url);
        IEnumerable<Article> GetArticleByName(string name);
        IEnumerable<Article> GetArticleByParentID(int id);
        IEnumerable<Article> GetAllPaging(int index, int page, string keywords);
        IEnumerable<Article> GetAll();
        void Save();
    }
    public class ArticleService : IArticleService
    {
        #region General
        IArticleRepository _articleRepository;
        IUnitOfWork _unitOfWork;

        public ArticleService(IArticleRepository articleRepository, IUnitOfWork unitOfWork)
        {
            this._articleRepository = articleRepository;
            this._unitOfWork = unitOfWork;
        }

        public void Add(Article Article)
        {
            _articleRepository.Add(Article);
        }

        public void Delete(Article Article)
        {
            _articleRepository.Delete(Article);
        }

        public IEnumerable<Article> GetAll()
        {
            return _articleRepository.GetAll();
        }

        public Article GetByID(int id)
        {
            return _articleRepository.GetSingleByCondition(x => x.ID == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Article Article)
        {
            _articleRepository.Update(Article);
        }
        #endregion

        #region Customize
        /// <summary>
        /// Get All of Articles By PrarentID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Article> GetArticleByParentID(int id)
        {
            return _articleRepository.GetMulti(a => a.ParentID.Equals(id));
        }

        public Article GetArticleByUrl(string url)
        {
            return _articleRepository.GetSingleByCondition(a => a.Url.Equals(url));
        }

        public IEnumerable<Article> GetArticleByName(string name)
        {
            return _articleRepository.GetMulti(a => a.Name.Contains(name));
        }
        public IEnumerable<Article> GetAllPaging(int index, int page, string keywords)
        {
            int total = 0;
            var articles = _articleRepository.GetMultiPaging(null, m => m.ID, out total, index, page);
            if (!string.IsNullOrEmpty(keywords))
                articles = articles.Where(m => m.Name.Contains(keywords));
            return articles.OrderByDescending(m => m.NodeOrder);
        }
        public IEnumerable<Article> CheckIsUrlExists(int id, string url)
        {
            if (id <= 0)
                return _articleRepository.GetMulti(m => m.Url.Equals(url));
            return _articleRepository.GetMulti(m => !m.ID.Equals(id) && m.Url.Equals(url));
        }
        #endregion
    }
}
