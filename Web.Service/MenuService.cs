﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;
using Web.Data.Repositories;
using Web.Data.Infrastructure;

namespace Web.Service
{
    public interface IMenuService
    {
        void Add(Menu menu);
        void Update(Menu menu);
        void Delete(Menu menu);
        Menu GetByID(int id);
        Menu GetMenuByUrl(string url);
        IEnumerable<Menu> CheckIsUrlExists(int id, string url);
        IEnumerable<Menu> GetMenuByLevelParent(int level, int parentID);
        IEnumerable<Menu> GetMenusByParent(int parentID);
        IEnumerable<Menu> GetMenuByParentNotObject(int parentID, int id);
        Menu GetMenuByLevelParentOrder(int level, int parentID, int nodeOrder);
        bool hasChangedParentID(Menu menu);
        IEnumerable<Menu> GetAll();
        IEnumerable<Menu> GetAllPaging(int index, int page, string keywords);
        IEnumerable<Menu> GetMenuByLevelParentNotObject(int level, int parentID, int id);
        void Save();
    }
    public class MenuService : IMenuService
    {
        #region General
        IMenuRepository _menuRepository;
        IUnitOfWork _unitOfWork;

        public MenuService(IMenuRepository menuRepository, IUnitOfWork unitOfWork)
        {
            this._menuRepository = menuRepository;
            this._unitOfWork = unitOfWork;
        }

        public void Add(Menu menu)
        {
            _menuRepository.Add(menu);
        }

        public void Delete(Menu menu)
        {
            _menuRepository.Delete(menu);
        }

        public IEnumerable<Menu> GetAll()
        {
            return _menuRepository.GetAll();
        }

        public Menu GetByID(int id)
        {
            return _menuRepository.GetSingleByCondition(x => x.ID == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Menu menu)
        {
            _menuRepository.Update(menu);
        }
        #endregion

        #region Customize
        public IEnumerable<Menu> GetMenuByParentNotObject(int parentID, int id)
        {
            return _menuRepository.GetMulti(m => m.ParentID == parentID && m.ID != id).OrderByDescending(m => m.NodeOrder);
        }

        public IEnumerable<Menu> GetMenuByLevelParentNotObject(int level, int parentID, int id)
        {
            return _menuRepository.GetMulti(m => m.Level == level && m.ParentID == parentID && m.ID != id).OrderByDescending(m => m.NodeOrder);
        }

        public Menu GetMenuByUrl(string url)
        {
            return _menuRepository.GetSingleByCondition(m => m.Url.Equals(url));
        }

        public IEnumerable<Menu> CheckIsUrlExists(int id, string url)
        {
            if (id <= 0)
                return _menuRepository.GetMulti(m => m.Url.Equals(url));
            return _menuRepository.GetMulti(m => !m.ID.Equals(id) && m.Url.Equals(url));
        }

        public bool hasChangedParentID(Menu menu)
        {
            var inpurParentID = menu.ParentID;
            var _menu = _menuRepository.GetSingleById(menu.ID);
            if(_menu == null)
            {
                return false;
            }
            if (inpurParentID != _menu.ID)
                return true;
            return false;
        }

        public IEnumerable<Menu> GetMenuByLevelParent(int level, int parentID)
        {
            return _menuRepository.GetMulti(m => m.Level == level && m.ParentID == parentID).OrderByDescending(m => m.NodeOrder);
        }

        public IEnumerable<Menu> GetMenusByParent(int parentID)
        {
            return _menuRepository.GetMulti(m => m.ParentID == parentID).OrderByDescending(m => m.NodeOrder);
        }

        public Menu GetMenuByLevelParentOrder(int level, int parentID, int nodeOrder)
        {
            return _menuRepository.GetSingleByCondition(m => m.Level == level && m.ParentID == parentID && m.NodeOrder == nodeOrder);
        }
        public IEnumerable<Menu> GetAllPaging(int index, int page, string keywords)
        {
            int total = 0;
            var menus = _menuRepository.GetMultiPaging(null, m => m.ID, out total, index, page);
            if(!string.IsNullOrEmpty(keywords))
               menus = menus.Where(m => m.Name.Contains(keywords));
            return menus.OrderByDescending(m => m.NodeOrder);
        }
        #endregion
    }
}
