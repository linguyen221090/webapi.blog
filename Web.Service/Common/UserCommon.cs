﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Data.Common;
using Web.Model.Models;

namespace Web.Service.Common
{
    public class UserCommon
    {
        private UserContext _userContext = new UserContext();
        
        public User ValidLogin(string username, string password)
        {
            return _userContext.ValidLogin(username, password);
        }
    }
}
