﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Data.Infrastructure;
using Web.Data.Repositories;
using Web.Model.Models;
using Web.Service.DTO;

namespace Web.Service
{
    public interface IPaymentService
    {
        IList<PaymentDto> GetAll();
        void Save();
    }

    public class PaymentService : IPaymentService
    {
        #region General
        IPaymentRepository _paymentRepository;
        IUnitOfWork _unitOfWork;


        public PaymentService(IPaymentRepository paymentRepository, IUnitOfWork unitOfWork)
        {
            this._paymentRepository = paymentRepository;
            this._unitOfWork = unitOfWork;
        }

        public IList<PaymentDto> GetAll()
        {
            string[] includes = new string[] { "User" };
            var result = _paymentRepository.GetAll(includes).ToList();
            return Mapper.Map<IList<Payment>, IList<PaymentDto>>(result);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
        #endregion
    }
}
