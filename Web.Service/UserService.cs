﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;
using Web.Data.Repositories;
using Web.Data.Infrastructure;

namespace Web.Service
{
    public interface IUserService
    {
        void Add(User user);
        void Update(User user);
        void Delete(User user);
        User GetByID(int id);
        IEnumerable<User> GetAll();
        IList<User> GetUsers();
        void Save();
    }

    public class UserService : IUserService
    {
        #region General
        IUserRepository _userRepository;
        IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            this._userRepository = userRepository;
            this._unitOfWork = unitOfWork;
        }

        public void Add(User user)
        {
            _userRepository.Add(user);
        }

        public void Delete(User user)
        {
            _userRepository.Delete(user);
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        public IList<User> GetUsers()
        {
            string[] includes = new string[] { "Role" };
            return _userRepository.GetAll(includes).ToList();
        }

        public User GetByID(int id)
        {
            return _userRepository.GetSingleByCondition(x => x.ID == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(User user)
        {
            _userRepository.Update(user);
        }
        #endregion
    }
}
