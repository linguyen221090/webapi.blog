﻿using System.Collections.Generic;

namespace Web.Service.DTO
{
    public class RoleDto
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
        public string RoleDisplayName { get; set; }
    }
}
