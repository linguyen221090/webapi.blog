﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Service.DTO
{
    public class ArticleRequestDto
    {
        public int ID { get; set; }
        public string Topic { get; set; }
        public int Number { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string File { get; set; }
        public string Website { get; set; }
        public string Note { get; set; }
        public int ARUserID { get; set; }
        public int ARPaymentID { get; set; }
        
        public UserDto User { get; set; }
        public PaymentDto Payment { get; set; }
    }
}
