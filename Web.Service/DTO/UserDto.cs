﻿using System;
using System.Collections.Generic;

namespace Web.Service.DTO
{
    public class UserDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime? Birthday { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string UserPassword { get; set; }

        public Guid GuiD { get; set; }

        public int UserRoleID { get; set; }

        public RoleDto Role { get; set; }

        public virtual ICollection<ArticleRequestDto> IArticleRequest { get; set; }
    }
}
