﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Service.Enum;

namespace Web.Service.DTO
{
    public class PaymentDto
    {
        public int ID { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }
        [NotMapped]
        public PaymentStatusTypeDto StatusEnum
        {
            get
            {
                return (PaymentStatusTypeDto)Status;
            }
            set
            {
                Status = (int)value;
            }
        }

        public string File { get; set; }

        public virtual ICollection<ArticleRequestDto> IArticleRequest { get; set; }
    }
}
