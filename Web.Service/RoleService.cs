﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Model.Models;
using Web.Data.Repositories;
using Web.Data.Infrastructure;

namespace Web.Service
{
    public interface IRoleSerice
    {
        void Add(Role role);
        void Update(Role role);
        void Delete(Role role);
        Role GetByID(int id);
        IEnumerable<Role> GetAll();
        IEnumerable<Role> GetListByName(string name);
        void Save();
    }
    public class RoleService : IRoleSerice
    {
        #region General
        IRoleRepository _roleRepository;
        IUnitOfWork _unitOfWork;

        public RoleService(IRoleRepository roleRepository, IUnitOfWork unitOfWork)
        {
            this._roleRepository = roleRepository;
            this._unitOfWork = unitOfWork;
        }

        public void Add(Role role)
        {
            _roleRepository.Add(role);
        }

        public void Delete(Role role)
        {
            _roleRepository.Delete(role);
        }

        public IEnumerable<Role> GetAll()
        {
            return _roleRepository.GetAll();
        }

        public Role GetByID(int id)
        {
            return _roleRepository.GetSingleByCondition(x => x.ID == id);
        }

        public IEnumerable<Role> GetListByName(string name)
        {
            return _roleRepository.GetListByName(name);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Role role)
        {
            _roleRepository.Update(role);
        }
        #endregion
    }
}
