﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Data.Infrastructure;
using Web.Data.Repositories;
using Web.Model.Models;
using Web.Service.DTO;

namespace Web.Service
{
    public interface IArticleRequestService
    {
        IList<ArticleRequestDto> GetAll();
        void Add(ArticleRequestDto articleRequestDto);
        void Update(ArticleRequestDto articleRequestDto);
        void Delete(int id);
        ArticleRequestDto GetByID(int id);
        void Save();
    }

    public class ArticleRequestService : IArticleRequestService
    {
        #region General
        IArticleRequestRepository _articleRequestRepository;
        IUnitOfWork _unitOfWork;


        public ArticleRequestService(IArticleRequestRepository articleRequestRepository, IUnitOfWork unitOfWork)
        {
            this._articleRequestRepository = articleRequestRepository;
            this._unitOfWork = unitOfWork;
        }

        public IList<ArticleRequestDto> GetAll()
        {
            var result = _articleRequestRepository.GetAll().ToList();
            return Mapper.Map<IList<ArticleRequest>, IList<ArticleRequestDto>>(result);
        }

        public void Add(ArticleRequestDto articleRequestDto)
        {
            var articleRequest = new ArticleRequest()
            {
                Topic = articleRequestDto.Topic,
                Number = articleRequestDto.Number,
                File = articleRequestDto.File,
                DateFrom = articleRequestDto.DateFrom,
                DateTo = articleRequestDto.DateTo,
                Website = articleRequestDto.Website,
                Note = articleRequestDto.Note,
                ARUserID = articleRequestDto.ARUserID,
                ARPaymentID = articleRequestDto.ARPaymentID
            };
            _articleRequestRepository.Add(articleRequest);
            Save();
        }

        public void Delete(int id)
        {
            var articleRequest = _articleRequestRepository.GetSingleById(id);
            _articleRequestRepository.Delete(articleRequest);
        }

        public void Update(ArticleRequestDto articleRequestDto)
        {
            var articleRequest = _articleRequestRepository.GetSingleByCondition(x => x.ID == articleRequestDto.ID);

            if (articleRequest == null)
                throw new ArgumentNullException("articleRequest is not found!");

            articleRequest.Topic = articleRequestDto.Topic;
            articleRequest.Number = articleRequestDto.Number;
            articleRequest.File = articleRequestDto.File;
            articleRequest.DateFrom = articleRequestDto.DateFrom;
            articleRequest.DateTo = articleRequestDto.DateTo;
            articleRequest.Website = articleRequestDto.Website;
            articleRequest.Note = articleRequestDto.Note;
            articleRequest.ARUserID = articleRequestDto.ARUserID;
            articleRequest.ARPaymentID = articleRequestDto.ARPaymentID;

            _articleRequestRepository.Update(articleRequest);
            Save();
        }

        public ArticleRequestDto GetByID(int id)
        {
            var result = _articleRequestRepository.GetSingleByCondition(x => x.ID == id);
            return Mapper.Map<ArticleRequest, ArticleRequestDto>(result);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
        #endregion
    }
}
