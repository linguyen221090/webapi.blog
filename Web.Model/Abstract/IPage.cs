﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Model.Abstract
{
    public interface IPage
    {
        string MetaTitle { get; set; }
        string MetaDescription { get; set; }
        string MetaKeyword { get; set; }
        string Url { get; set; }
        DateTime? PublicFrom { get; set; }
        DateTime? PublicTo { get; set; }
        int? CreatedByUser { get; set; }
        int? ModifiedByUser { get; set; }
        DateTime WhenCreated { get; set; }
        DateTime? WhenModified { get; set; }
        bool IsDeleted { get; set; }
        int ParentID { get; set; }
        int Level { get; set; }
    }
}
