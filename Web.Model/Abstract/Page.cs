﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Model.Abstract
{
    public abstract class Page: IPage
    {
        private DateTime _date = DateTime.Now;

        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        [Required]
        public string Url { get; set; }
        
        public DateTime? PublicFrom { get; set; }
        public DateTime? PublicTo { get; set; }

        public int? CreatedByUser { get; set; }
        public int? ModifiedByUser { get; set; }
        public DateTime WhenCreated
        {
            get { return _date; }
            set { _date = value; }
        }
        public DateTime? WhenModified { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public int ParentID { get; set; }
        public int NodeOrder { get; set; }
        public int Level { get; set; }
    }
}
