﻿namespace Web.Model.Enum
{
    public enum DataActionType
    {
        Error = 0,
        Success = 1
    }
}
