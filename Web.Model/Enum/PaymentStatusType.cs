﻿namespace Web.Model.Enum
{
    public enum PaymentStatusType
    {
        NoPaid = 0,
        Paid = 1,
        Refund = 2
    }
}
