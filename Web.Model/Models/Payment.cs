﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Model.Enum;

namespace Web.Model.Models
{
    public class Payment
    {
        private DateTime _date = DateTime.Now;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }
        [NotMapped]
        public PaymentStatusType StatusEnum
        {
            get
            {
                return (PaymentStatusType)Status;
            }
            set
            {
                Status = (int)value;
            }
        }

        public string File { get; set; }

        public int? CreatedByUser { get; set; }
        public int? ModifiedByUser { get; set; }
        public DateTime WhenCreated
        {
            get { return _date; }
            set { _date = value; }
        }
        public DateTime? WhenModified { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public virtual ICollection<ArticleRequest> IArticleRequest { get; set; }
    }
}
