﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Web.Model.Models
{
    [Table("User")]
    public class User
    {
        private DateTime _date = DateTime.Now;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(255)]
        [Required]
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(100)]
        public string UserName { get; set; }
        [StringLength(100)]
        public string UserPassword { get; set; }
        public Guid GuiD { get; set; }
        public int UserRoleID { get; set; }

        public int? CreatedByUser { get; set; }
        public int? ModifiedByUser { get; set; }
        public DateTime WhenCreated
        {
            get { return _date; }
            set { _date = value; }
        }
        public DateTime? WhenModified { get; set; } 
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        [ForeignKey("UserRoleID")]
        public Role Role { get; set; }

        public virtual ICollection<ArticleRequest> IArticleRequest { get; set; }
    }
}
