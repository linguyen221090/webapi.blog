﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Model.Models
{
    public class ArticleRequest
    {
        private DateTime _date = DateTime.Now;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(500)]
        [Required]
        public string Topic { get; set; }
        public int Number { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string File { get; set; }
        public string Website { get; set; }
        public string Note { get; set; }
        public int? ARPaymentID { get; set; }
        public int? ARUserID { get; set; }

        public int? CreatedByUser { get; set; }
        public int? ModifiedByUser { get; set; }
        public DateTime WhenCreated
        {
            get { return _date; }
            set { _date = value; }
        }
        public DateTime? WhenModified { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        [ForeignKey("ARPaymentID")]
        public virtual Payment Payment { get; set; }

        [ForeignKey("ARUserID")]
        public virtual User User { get; set; }
    }
}
