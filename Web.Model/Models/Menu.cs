﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Model.Abstract;

namespace Web.Model.Models
{
    [Table("Menu")]
    public class Menu: Page
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Icon { get; set; }
        public int Group { get; set; }
        public string Text { get; set; }
    }
}
