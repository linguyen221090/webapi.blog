﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Model.Abstract;

namespace Web.Model.Models
{
    [Table("Article")]
    public class Article: Page
    {
        public int ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Teaser { get; set; }
        public DateTime DateRelease { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        public string Text { get; set; }
    }
}
