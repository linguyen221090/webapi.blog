﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class RoleViewModels
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
        public string RoleDisplayName { get; set; }
    }
}