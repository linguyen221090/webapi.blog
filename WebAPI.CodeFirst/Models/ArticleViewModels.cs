﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class ArticleViewModels
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Teaser { get; set; }
        public string Description { get; set; }
        public DateTime DateRelease { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public int ParentID { get; set; }
        public int NodeOrder { get; set; }
        public int Level { get; set; }
    }
}