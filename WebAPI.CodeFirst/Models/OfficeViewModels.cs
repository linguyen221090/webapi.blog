﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class OfficeViewModels
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Nation { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Copyright { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
    }
}