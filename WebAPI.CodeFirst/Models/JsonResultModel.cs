﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class JsonResultModel
    {
        public JsonResultModel()
        {
            ErrorCode = 200;
        }
        public int ErrorCode { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public string JsonData { get; set; }
    }
}