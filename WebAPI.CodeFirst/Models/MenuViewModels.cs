﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class MenuViewModels
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public int Group { get; set; }
        public string Url { get; set; }
        public int Level { get; set; }
        public int ParentID { get; set; }
        public int NodeOrder { get; set; }
        public string Text { get; set; }
    }
}