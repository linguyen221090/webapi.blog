﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Common.Helpers;
using Web.Model.Enum;
using WebAPI.CodeFirst.Models;

namespace WebAPI.CodeFirst.Controllers
{
    public class ApiControllerBase : ApiController
    {
        protected IHttpActionResult JsonResult(int errorCode, bool status, DataActionType message)
        {
            var result = new JsonResultModel { ErrorCode = errorCode, Status = status, Message = MessageHelper.GetReturnMessage(message) };
            return Json(result, new JsonSerializerSettings());
        }
    }
}
