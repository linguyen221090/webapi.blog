﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.CodeFirst.Models;
using Web.Model.Models;
using Web.Service;
using AutoMapper;
using System.Text;
using System.Net.Http.Headers;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace WebAPI.CodeFirst.Controllers
{
    [Authorize]
    [RoutePrefix("api/role")]
    public class RoleController : ApiController
    {
        IRoleSerice _roleService;

        public RoleController()
        {

        }

        public RoleController(IRoleSerice roleService)
        {
            this._roleService = roleService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<Role> GetAll()
        {
            var roles = _roleService.GetAll();
            if (roles == null || roles.Count() == 0)
            {
                return null;
            }
            return roles;
        }

        [HttpPut]
        public IHttpActionResult UpdateRole(Role role)
        {
            if (ModelState.IsValid)
            {
                _roleService.Update(role);
                _roleService.Save();
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteRole(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            var role = _roleService.GetByID(id);
            if (role == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _roleService.Delete(role);
                _roleService.Save();
            }
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult CreateRole(Role role)
        {
            if (ModelState.IsValid)
            {
                _roleService.Add(role);
                _roleService.Save();
            }
            return Ok();
        }

        [HttpGet]
        [Route("{id:int}")]
        public IHttpActionResult GetRole(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            var role = _roleService.GetByID(id);
            if (role == null)
                return NotFound();
            return Ok(role);
        }

        [HttpGet]
        [Route("search/{name}")]
        [AllowAnonymous]
        public IEnumerable<Role> GetListByName(string name)
        {
            var roles = _roleService.GetListByName(name);
            if (roles == null || roles.Count() == 0)
            {
                return null;
            }
            return roles;
        }

    }
}
