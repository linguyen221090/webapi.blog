﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Model.Models;
using Web.Service;
using WebAPI.CodeFirst.Models;

namespace WebAPI.CodeFirst.Controllers
{
    [Authorize]
    [RoutePrefix("api/article")]
    public class ArticleController : ApiController
    {
        IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            this._articleService = articleService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<ArticleViewModels> GetArticles()
        {
            var articles = _articleService.GetAll();
            if (articles == null)
                return null;
            var viewModels = Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleViewModels>>(articles);
            return viewModels;
        }

        [HttpPut]
        public IHttpActionResult UpdateArticle(Article article)
        {
            if (ModelState.IsValid)
            {
                _articleService.Update(article);
                _articleService.Save();
            }
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult CreateArticle(Article article)
        {
            if (ModelState.IsValid)
            {
                _articleService.Add(article);
                _articleService.Save();
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteArticle(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            var article = _articleService.GetByID(id);
            if (article == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _articleService.Delete(article);
                _articleService.Save();
            }
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("search/{name}")]
        public IEnumerable<ArticleViewModels> GetArticleByName(string name)
        {
            var articles = _articleService.GetArticleByName(name);
            if (articles == null || articles.Count() == 0)
                return null;
            var viewModel = Mapper.Map< IEnumerable<Article>, IEnumerable<ArticleViewModels>>(articles);
            return viewModel;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("parentid/{id:int}")]
        public IEnumerable<ArticleViewModels> GetArticleByParentID(int id)
        {
            var articles = _articleService.GetArticleByParentID(id);
            if (articles == null || articles.Count() == 0)
                return null;
            var viewModel = Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleViewModels>>(articles);
            return viewModel;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("url/{url}")]
        public ArticleViewModels GetArticleByUrl(string url)
        {
            var _url = string.Format("/{0}", url);
            var article = _articleService.GetArticleByUrl(_url);
            if (article == null)
                return null;
            var viewModel = Mapper.Map<Article, ArticleViewModels>(article);
            return viewModel;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("paging/{index:int?}/{page:int?}/{keywords?}")]
        public IEnumerable<ArticleViewModels> GetArticlesPaging(int index = 0, int page = 20, string keywords = "")
        {
            IEnumerable <Article> articles = _articleService.GetAllPaging(index, page, keywords);
            if (articles == null)
            {
                return null;
            }

            var viewModels = Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleViewModels>>(articles);

            return viewModels;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("exists/{id:int}/{url?}")]
        public IHttpActionResult IsUrlExists(int id = -1, string url = "")
        {
            var _url = string.Format("/{0}", url);
            var articles = _articleService.CheckIsUrlExists(id, _url);
            if (articles == null || articles.Count() == 0)
                return Ok(false);
            return Ok(true);
        }
    }
}
