﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Model.Models;
using Web.Service;
using WebAPI.CodeFirst.Models;

namespace WebAPI.CodeFirst.Controllers
{
    [RoutePrefix("api/office")]
    [Authorize]
    public class OfficeController : ApiController
    {
        IOfficeService _officeService;

        public OfficeController(IOfficeService officeService)
        {
            this._officeService = officeService;
        }

        [HttpGet]
        [Route("{id:int}")]
        [AllowAnonymous]
        public IHttpActionResult GetOfficeByID(int id = 0)
        {
            if (id <= 0)
                return BadRequest();
            var office = _officeService.GetByID(id);
            if (office == null)
                return Ok();
            var viewModels = Mapper.Map<Office, OfficeViewModels>(office);
            return Ok(viewModels);
        }

        [HttpPut]
        public IHttpActionResult UpdateOffice(Office office)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _officeService.Update(office);
            _officeService.Save();
            return Ok();
        }
    }
}
