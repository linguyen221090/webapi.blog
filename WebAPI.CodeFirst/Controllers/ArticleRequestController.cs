﻿using System;
using System.Web.Http;
using Web.Model.Enum;
using Web.Service;
using Web.Service.DTO;

namespace WebAPI.CodeFirst.Controllers
{
    [Authorize]
    [RoutePrefix("api/articlerequest")]
    public class ArticleRequestController : ApiControllerBase
    {
        IArticleRequestService _articleRequestService;

        public ArticleRequestController(IArticleRequestService articleRequestService)
        {
            this._articleRequestService = articleRequestService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetArticleRequests()
        {
            try
            {
                var result = _articleRequestService.GetAll();
                if (result == null)
                {
                    return JsonResult(500, false, DataActionType.Error);
                }
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPut]
        public IHttpActionResult UpdateArticleRequest(ArticleRequestDto articleRequestDto)
        {
            if (ModelState.IsValid)
            {
                _articleRequestService.Update(articleRequestDto);
            }
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult CreateArticleRequest(ArticleRequestDto articleRequestDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _articleRequestService.Add(articleRequestDto);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteArticleRequest(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            try
            {
                if (ModelState.IsValid)
                {
                    _articleRequestService.Delete(id);
                    _articleRequestService.Save();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
