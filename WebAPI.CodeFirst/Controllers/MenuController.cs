﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Service;
using Web.Model.Models;
using WebAPI.CodeFirst.Models;
using Web.Model.Enum;

namespace WebAPI.CodeFirst.Controllers
{
    [RoutePrefix("api/menu")]
    [Authorize]
    public class MenuController : ApiControllerBase
    {
        IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            this._menuService = menuService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<MenuViewModels> GetMenus()
        {
            IEnumerable<Menu> menus = _menuService.GetAll();
            if (menus == null)
            {
                return null;
            }

            var viewModels = Mapper.Map<IEnumerable<Menu>, IEnumerable<MenuViewModels>>(menus);

            return viewModels;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("paging/{index:int?}/{page:int?}/{keywords?}")]
        public IHttpActionResult GetMenusPaging(int index = 0, int page = 20, string keywords = "")
        {
            IList<Menu> menus = _menuService.GetAllPaging(index, page, keywords).ToList();
            if (menus == null)
            {
                return null;
            }

            var viewModels = Mapper.Map<IList<Menu>, IList<MenuViewModels>>(menus);

            return Json(viewModels);
        }

        [HttpPut]
        public IHttpActionResult UpdateMenu(Menu menu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _menuService.Update(menu);
                    _menuService.Save();
                }
                return JsonResult(200, true, DataActionType.Success);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("putChangeParent")]
        public IHttpActionResult UpdateMenuAndParent(Menu menu)
        {
            try
            {
                #region Get Highest Order Object with Level and ParentID same.
                var menus = _menuService.GetMenuByParentNotObject(menu.ParentID, menu.ID);
                if (menus == null || menus.Count() == 0)
                {
                    var parent = _menuService.GetByID(menu.ParentID);
                    menu.Level = parent.Level + 1;
                    menu.NodeOrder = 1;
                }
                else
                {
                    menu.NodeOrder = menus.FirstOrDefault().NodeOrder + 1;
                    menu.Level = menus.FirstOrDefault().Level;
                }
                #endregion

                if (ModelState.IsValid)
                {
                    _menuService.Update(menu);
                    _menuService.Save();
                }
                return JsonResult(200, true, DataActionType.Success);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IHttpActionResult CreateMenu(Menu menu)
        {
            try
            {
                #region Get Highest Order Object with Level and ParentID same.
                var menus = _menuService.GetMenuByParentNotObject(menu.ParentID, menu.ID);
                if (menus == null || menus.Count() == 0)
                {
                    var parent = _menuService.GetByID(menu.ParentID);
                    menu.Level = parent.Level + 1;
                    menu.NodeOrder = 1;
                }
                else
                {
                    menu.NodeOrder = menus.FirstOrDefault().NodeOrder + 1;
                    menu.Level = menus.FirstOrDefault().Level;
                }
                #endregion

                if (ModelState.IsValid)
                {
                    _menuService.Add(menu);
                    _menuService.Save();
                }
                return JsonResult(200, true, DataActionType.Success);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteMenu(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest("Not a valid user id");
                var menu = _menuService.GetByID(id);
                if (menu == null)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    _menuService.Delete(menu);
                    _menuService.Save();
                }
                return JsonResult(200, true, DataActionType.Success);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("order/{e:bool}")]
        public IHttpActionResult UpdateNodeOrderMenu(Menu menu, bool e)
        {
            var _level = menu.Level;
            var _parentID = menu.ParentID;
            var _nodeOrder = menu.NodeOrder;
            Menu _replaceMenu = new Menu();
            if (e == true)//change to up
            {
                var topMenu = _menuService.GetMenuByLevelParentNotObject(_level, _parentID, menu.ID);
                if (topMenu == null || topMenu.Count() == 0)
                    return NotFound();
                if (_nodeOrder > topMenu.FirstOrDefault().NodeOrder)
                    return NotFound();
                _nodeOrder++;
                _replaceMenu = _menuService.GetMenuByLevelParentOrder(_level, _parentID, _nodeOrder);
                if (_replaceMenu != null)
                    _replaceMenu.NodeOrder--;
                menu.NodeOrder++;
            }
            else if (e == false)//change to down
            {
                if (menu.NodeOrder == 1)
                    return BadRequest();
                _nodeOrder--;
                _replaceMenu = _menuService.GetMenuByLevelParentOrder(_level, _parentID, _nodeOrder);
                if (_replaceMenu != null)
                    _replaceMenu.NodeOrder++;
                menu.NodeOrder--;
            }
            if (ModelState.IsValid)
            {
                _menuService.Update(menu);
                if (_replaceMenu != null)
                    _menuService.Update(_replaceMenu);
                _menuService.Save();
            }
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("level/{level:int}/{parentID:int}")]
        public IEnumerable<MenuViewModels> GetMenuByLevelParent(int level = -1, int parentID = -1)
        {
            if (level == -1 || parentID == -1)
                return null;
            IEnumerable<Menu> menus = _menuService.GetMenuByLevelParent(level, parentID);
            if (menus == null)
            {
                return null;
            }

            var viewModels = Mapper.Map<IEnumerable<Menu>, IEnumerable<MenuViewModels>>(menus);

            return viewModels;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("url/{url}")]
        public MenuViewModels GetMenuByUrl(string url)
        {
            var _url = string.Format("/{0}", url);
            var menu = _menuService.GetMenuByUrl(_url);
            if (menu == null)
                return null;
            var viewModel = Mapper.Map<Menu, MenuViewModels>(menu);
            return viewModel;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("exists/{id:int}/{url?}")]
        public IHttpActionResult IsUrlExists(int id = -1, string url = "")
        {
            var _url = string.Format("/{0}", url);
            var menus = _menuService.CheckIsUrlExists(id, _url);
            if (menus == null || menus.Count() == 0)
                return Ok(false);
            return Ok(true);
        }

        [HttpGet]
        [Route("sum/{id:int}")]
        [AllowAnonymous]
        public IHttpActionResult GetSumMenus(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            var menu = _menuService.GetMenusByParent(id);
            if (menu == null)
                return NotFound();
            return Ok(menu.Count());
        }
    }
}
