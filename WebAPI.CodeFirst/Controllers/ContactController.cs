﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using Web.Common.Helpers;
using Web.Model.Models;
using Web.Service;

namespace WebAPI.CodeFirst.Controllers
{
    [RoutePrefix("api/contact")]
    public class ContactController : ApiController
    {
        IOfficeService _officeService;

        public ContactController(IOfficeService officeService)
        {
            this._officeService = officeService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> SendMailContactAndMailThanks(ContactUser contactUser)
        {
            try
            {
                MailHelper mailHelper = new MailHelper(MailHelper.EMAIL_SENDER, MailHelper.EMAIL_CREDENTIALS, MailHelper.SMTP_CLIENT);
                var emailBody = String.Format(MailHelper.EMAIL_BODY, contactUser.Name, contactUser.Phone, contactUser.Email, contactUser.Subject, contactUser.Message);
                if (mailHelper.SendEMail(contactUser.Email, contactUser.Subject, emailBody))
                {
                    //
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok();
        }
    }
}
