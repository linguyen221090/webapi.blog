﻿using System.Web.Http;
using Web.Model.Enum;
using Web.Service;

namespace WebAPI.CodeFirst.Controllers
{
    [Authorize]
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiControllerBase
    {
        IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService)
        {
            this._paymentService = paymentService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetPayments()
        {
            var result = _paymentService.GetAll();
            if (result == null)
            {
                return JsonResult(500, false, DataActionType.Error);
            }
            return Json(result);
        }
    }
}
