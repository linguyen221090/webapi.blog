﻿using AutoMapper;
using Web.Model.Models;
using Web.Service.DTO;
using WebAPI.CodeFirst.Models;

namespace WebAPI.CodeFirst.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<User, UserViewModels>();
                cfg.CreateMap<Menu, MenuViewModels>();
                cfg.CreateMap<Article, ArticleViewModels>();
                cfg.CreateMap<Office, OfficeViewModels>();
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<Payment, PaymentDto>();
                cfg.CreateMap<ArticleRequest, ArticleRequestDto>();
                cfg.CreateMap<ArticleRequestDto, ArticleRequest>();
            });
        }

    }
}